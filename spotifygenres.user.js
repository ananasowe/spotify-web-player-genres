// ==UserScript==
// @name        Music genre display for Spotify
// @name:fr     Affichage des genres musicaux pour Spotify
// @name:pl     Wyświetlanie gatunków muzycznych dla Spotify
// @namespace   spotifywebapi_genres
// @match       https://open.spotify.com/*
// @grant       GM_xmlhttpRequest
// @version     1.0
// @supportURL   https://gitlab.com/ananasowe/spotify-web-player-genres/-/issues
// @description  Fetch music genres from Spotify Web API and display below the artist name
// @description:fr  Récupérer les genres musicaux depuis l'API Web de Spotify et les afficher sous le nom de l'artiste
// @description:pl  Pobieranie gatunków muzycznych ze Spotify Web API i wyświetlanie ich pod nazwą wykonawcy
// @author      ananasowe
// @downloadURL https://gitlab.com/ananasowe/spotify-web-player-genres/-/raw/main/spotifygenres.user.js
// ==/UserScript==

(function() {
    'use strict';

    const tokenEndpoint = 'https://accounts.spotify.com/api/token';
    const endpointMarket = "FR";

    let clientId;
    let clientSecret;

    let currentAccessToken;
    let genrePreviewElement;


    // Function to set up Spotify API credentials
    function setupSpotifyCredentials() {
        clientId = localStorage.getItem('spotify_client_id');
        clientSecret = localStorage.getItem('spotify_client_secret');

        if (!(clientId && clientSecret)) {
          clientId = prompt('Please enter your Spotify Web API Client ID:');
          clientSecret = prompt('Please enter your Spotify Web API Client Secret token:');

          if (clientId && clientSecret) {
              localStorage.setItem('spotify_client_id', clientId);
              localStorage.setItem('spotify_client_secret', clientSecret);
          } else {
              window.alert('Please provide both Client ID and Client Secret to be used for fetching genres from Spotify API');
              setupSpotifyCredentials(); // Loop until Spotify API credentials will be availabe
          }
        }
    }

    setupSpotifyCredentials();

    // Save the Spotify API access token and its expiration date to local storage
    function saveAccessToken(token, expiresIn) {
        const expirationDate = new Date(Date.now() + expiresIn * 1000);
        localStorage.setItem('spotify_access_token', token);
        localStorage.setItem('spotify_token_expiration', expirationDate.toISOString());
        console.log('Access token saved:', token, 'Expires at:', expirationDate);
    }

    // Obtain a new Spotify API session token
    function requestAccessToken() {
      GM_xmlhttpRequest({
          method: 'POST',
          url: tokenEndpoint,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
              'Authorization': 'Basic ' + btoa(clientId + ':' + clientSecret),
          },
          data: 'grant_type=client_credentials',
          onload: function(response) {
              const jsonResponse = JSON.parse(response.responseText);
              if (jsonResponse.access_token) {
                  saveAccessToken(jsonResponse.access_token, jsonResponse.expires_in);
              }
          },
          onerror: function(error) {
              console.error(error);
          }
      });
    }

    function isTokenValid() {
        const tokenExpiration = localStorage.getItem('spotify_token_expiration');
        return tokenExpiration && new Date(tokenExpiration) > new Date();
    }

    function refreshToken() {
      // Check if a valid token is already saved in local storage
      if (!isTokenValid()) {
          console.log('No valid token found. Requesting a new one…');
          requestAccessToken();
      }
      currentAccessToken = localStorage.getItem('spotify_access_token');
    }

    refreshToken();

    function linkUpdated(href) {
        const decodedHref = decodeURIComponent(href);
        const searchString = 'spotify:track:';
        const trackIndex = decodedHref.indexOf(searchString);
        if (trackIndex !== -1) {
            // Extract anything that follows the ":" in the "track:" sequence
            const track = decodedHref.substring(trackIndex + searchString.length);
            fetchTrack(track);
        }
    }

    function fetchTrack(track) {
        fetchTrackInfo(track, currentAccessToken).then(artistIds => {
          return artistIds;
        }).then(artistIds => {
          return Promise.all(artistIds.map(artistId => fetchArtists(artistId, currentAccessToken)));
        }).then(genreList => {
          let genres = [...new Set(genreList)].flat(2).join(', ');
          console.log("Resolved genres: " + genres);

          if (genrePreviewElement) {
            genrePreviewElement.remove();
          }
          const nowPlayingWidget = document.querySelector('[data-testid="now-playing-widget"]');
          const gridElement = nowPlayingWidget.children[1];
          const subtitlesDiv = gridElement.lastElementChild;
          const addedSubtitlesDiv = subtitlesDiv.cloneNode(true);
          addedSubtitlesDiv.classList = []
          genrePreviewElement = addedSubtitlesDiv; // Preserve reference to genre preview
          const spanElement = document.createElement("span");
          spanElement.textContent = genres;
          spanElement.onclick = function() { window.alert(genres); };
          const spanWrapper = addedSubtitlesDiv.querySelector('span').parentNode;
          spanWrapper.replaceChildren(spanElement);
          gridElement.appendChild(addedSubtitlesDiv);
        }).catch(error => {
                console.error('Error fetching track information:', error);
        });;
    }

    function fetchTrackInfo(trackId, accessToken) {
        return new Promise((resolve, reject) => {
            const apiUrl = `https://api.spotify.com/v1/tracks/${trackId}?market=${endpointMarket}`;

            GM_xmlhttpRequest({
                method: 'GET',
                url: apiUrl,
                headers: {
                    'Authorization': `Bearer ${accessToken}`,
                    'Content-Type': 'application/json',
                },
                onload: function(response) {
                    const jsonResponse = JSON.parse(response.responseText);

                    const artistsList = jsonResponse.artists.map(artist => artist.name);
                    const artistsIds = jsonResponse.artists.map(artist => artist.id);

                    resolve(artistsIds);
                },
                onerror: function(error) {
                    console.error(error);
                    reject(error);
                }
            });
        });
    }

    function fetchArtists(artistId, accessToken) {
        return new Promise((resolve, reject) => {
            const apiUrl = `https://api.spotify.com/v1/artists/${artistId}`;

            GM_xmlhttpRequest({
                method: 'GET',
                url: apiUrl,
                headers: {
                    'Authorization': `Bearer ${accessToken}`,
                    'Content-Type': 'application/json',
                },
                onload: function(response) {

                    const jsonResponse = JSON.parse(response.responseText);
                    const genresList = jsonResponse.genres;

                    resolve(genresList);
                },
                onerror: function(error) {
                    console.error(error);
                    reject(error);
                }
            });
        });
    }


    // Function to be called when the div is modified
    function updateInfo() {
        console.log('Now Playing Widget updated!');
        // Find a song link within the updated div with a specific href
        const updatedDiv = document.querySelector('[data-testid="now-playing-widget"]');
        if (updatedDiv) {
            const linkElement = updatedDiv.querySelector('a[href]');
            if (linkElement) {
                const newLink = linkElement.href;
                linkUpdated(newLink);
            }
        }
    }

    function throttle(func, wait) {
        let canRun = true;
        return function() {
            if (canRun) {
                func.apply(this, arguments);
                canRun = false;
                setTimeout(() => {
                    canRun = true;
                }, wait);
            }
        };
    }

    // Set up the main MutationObserver once the now-playing-widget appears
    function setupMainObserver(targetNode) {
        const observer = new MutationObserver(throttle(updateInfo, 500)); // Throttle update listener to once every 500 milliseconds
        const config = { childList: true, subtree: true, attributes: true };
        observer.observe(targetNode, config);
    }

    // Set up the initial MutationObserver to detect the first appearance of the now-playing-widget
    function setupObserver() {
        // Create a MutationObserver to watch for the insertion of elements with data-testid="now-playing-widget"
        const observer = new MutationObserver((mutationsList) => {
            for (const mutation of mutationsList) {
                if (mutation.type === 'childList' && mutation.addedNodes.length > 0) {
                    const addedNode = mutation.addedNodes[0];
                    if (addedNode && addedNode.nodeType === 1 && addedNode.matches('[data-testid="now-playing-widget"]')) {
                        setupMainObserver(addedNode.firstChild);
                        console.log("Set up main observer for now-playing-widget");
                        observer.disconnect(); // Initial observer will not be needed later
                        break;
                    }
                }
            }
        });
        const config = { childList: true, subtree: true };
        observer.observe(document.body, config);
    }

    setupObserver();
    console.log("Waiting for the widget to appear…")
})();
