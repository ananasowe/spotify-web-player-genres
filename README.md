# Spotify Web Player Genres

This Tempermonkey/Violentmonkey script allows for fetching music genres from Spotify Web API and displaying them below the artist name.
To install the script, click [here](https://gitlab.com/ananasowe/spotify-web-player-genres/-/raw/main/spotifygenres.user.js).